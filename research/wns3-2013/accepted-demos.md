---
layout: page
title: Accepted Posters/Demos
permalink: /research/wns3/wns3-2013/accepted-posters-demos-short-talks/
---
# Posters

  * **IEEE 802.11 transmission between two ns-3 applications over real interfaces using EmuNetDevice** ([pptx](http://www.nsnam.org/wp-content/uploads/2013/01/WNS3-2013-Poster-Deronne.pptx))

    S. Deronne, L. Salingros, V. Moeyaert, S. Bette
  * **Simulating opportunistic network protocols at mass events with ns-3**

    Bram Bonné, Arno Barzan, Peter Quax, Wim Lamotte
  * **Ey-Wifi: Active Signaling for the ns-3 802.11 Model**

    Hana Baccouch, Cedric Adjih, Paul Muhlethaler

# Demos
TBA
