---
layout: page
title: Information for Authors
permalink: /research/wns3/wns3-2013/info-for-authors/
---
# Submission guidelines

Papers must be written in English and must not exceed 8 pages. Authors should submit papers through [EasyChair](https://www.easychair.org/conferences/?conf=wns32013) in PDF format, complying with the ACM conference proceedings format. Submitted papers must not have been submitted for review or published (partially or completely) elsewhere. Every paper will be peer-reviewed. At least one author of each accepted paper must register and present the work at the conference.

All papers must strictly comply with <a href="http://www.acm.org/sigs/publications/proceedings-templates" rel="nofollow">ACM Conference Proceedings</a> format, Option 2 (tighter &#8216;Alternate&#8217; style).

# Camera-ready guidelines

Please see <a href="http://simutools.org/2013/index.php?n=Publication.CameraReadyInstructions" rel="nofollow">SIMUTools 2013 guidelines</a>.

# ICST copyright policy

[Copyright form (pdf)](http://www.nsnam.org/wp-content/uploads/2012/10/ICST_Copyright_Form.pdf)
