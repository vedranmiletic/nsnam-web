---
title: Training
layout: page
permalink: /research/wns3/wns3-2017/training/
---
ns-3 training sessions are fee-based events intended to both educate new users and raise funding for the project. Training is held one day prior to the Workshop on ns-3. One day of training is planned for the <a href="https://ns3-annual-2017.eventzilla.net/" target="_blank">ns-3 Consortium annual meeting</a> on June 12, 2017, in Porto. **<a href="https://www.nsnam.org/docs/consortium/training/ns-3-training-2017.pdf" target="_blank">Training flyer</a>**
