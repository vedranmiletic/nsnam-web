---
title: Download
layout: page
permalink: /releases/ns-3-29/download/
---
Please click the following link to download ns-3.29, released September 2018: 

  * [ns-allinone-3.29](/releases/ns-allinone-3.29.tar.bz2) (compressed source code archive)

A source code patch to update ns-3.28.1 release to ns-3.28 release is available [here](/release/patches/ns-3.28.1-to-ns-3.29.patch). Other patches to migrate older versions of ns-3 (back to ns-3.17) to the latest version can be found in the same directory; they must be applied sequentially to upgrade across multiple releases.
