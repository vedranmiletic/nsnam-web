---
title: FAQ
layout: page
permalink: /support/faq/
---
If you intend to use the ns-3 simulation libraries to explore the behavior of existing models by writing new simulation scripts in C++ or python, the following sections attempt to answer common questions that we see regularly on our mailing-lists. If you have questions that are not answered here, consider looking at our fairly extensive [documentation](/documentation) or asking your question on our [user mailing-list](/support/mailing-list).

  * [ns-2 & ns-3](/support/faq/ns2-ns3): high-level questions about how ns-3 relates to ns-2
  * [Setup](/support/faq/setup): how to download ns-3 and setup a working environment on your local machine
  * [Running scripts](/support/faq/running-scripts): how to run existing simulation scripts
  * [Writing scripts](/support/faq/writing-scripts): how to write new simulation scripts
  * [Miscellaneous](/support/faq/miscellaneous): common programming questions related to using ns-3
