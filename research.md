---
layout: page
title: Research
permalink: /research/
---
ns-3 has been used in thousands of publications to date.  To get a feel for the topics studied, search on the keywords 'ns-3 simulator' at the following:

* [Google Scholar](https://scholar.google.com) 
* [IEEE digital library](https://ieeexplore.ieee.org)
* [ACM digital library](https://dl.acm.org) 

The project organizes an annual meeting, the [Workshop on ns-3](https://www.nsnam.org/research/wns3/wns3-2019/), where advances on ns-3 itself are reported.  This year, it will be held in Florence, Italy, during a week-long meeting from June 17-21.
