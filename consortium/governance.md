---
title: Governance
layout: page
permalink: /consortium/governance/
---
The authoritative document that describes the structure and legal obligations of the NS-3 Consortium is the [NS-3 Consortium Establishment Agreement](/wp-content/uploads/2013/01/NS-3-Consortium-Establishment-Agreement.pdf) that has been signed by the University of Washington and the INRIA. Some corrections to this document were made in the following [amendment](http://www.nsnam.org/wp-content/uploads/2013/06/NS-3-Consortium-Amendment.pdf). The following text provides a high-level overview of the way the NS-3 Consortium works.

<a href="http://www.nsnam.org/consortium/governance/attachment/consortium-2/" rel="attachment wp-att-899"><img class="alignnone size-full wp-image-899" title="ns-3 Consortium Overview" src="http://www.nsnam.org/wp-content/uploads/2013/01/governance1.jpg" alt="ns-3 Consortium Overview" height="320" /></a>

# Steering Committee

The Steering Committee is initially composed of six directors, including one rotating Executive Director serving for a term of one year, all appointed by Executive Members. Each Founding Executive Member appoints three Directors. The Executive Director will rotate between Executive Members.

In addition to general management and oversight of the Consortium, the Steering Committee responsibilities shall include:

  * Supervising the roadmap of the software maintenance that it funds.
  * Making decisions on how the Consortium&#8217;s funds shall be expended, accordingly with its objectives (organization of training courses, workshops, etc.).
  * Organizing the various events in relation to the NS-3 Consortium.
  * Receiving and collecting Consortium Members&#8217; feedback and suggestions concerning NS-3 software development.
  * Submit suggestions made by Consortium Members to NS-3 Maintainers.
  * Preparing a yearly scientific, technical and financial report.
  * Accepting new Executive Members

The current Steering Committee consists of:

  * Tom Henderson (Executive Director, University of Washington)
  * Sumit Roy (University of Washington)
  * David Rey (INRIA)
  * Walid Dabbous (INRIA)
  * Damien Saucez (INRIA)
  * Lorenza Giupponi (CTTC)
  * Manuel Ricardo (INESC Porto)
  * Doug Blough (Georgia Institute of Technology)
  * Felipe Perrone (Bucknell University)

# Executive Members

Additional institutions may be be invited to become Executive Members by the Steering Committee.

Executive Members who are not Founding Executive Members (UW and INRIA) may appoint one individual to serve on the Steering Committee.

# Consortium Members

Not-for-profit organizations, small, and large companies may [apply](/consortium/memberships/join) to become Consortium Members. Each Consortium Member may:

  * Submit suggestions, requests and feedback concerning the ns-3 software development directions and roadmap, to be discussed during the yearly plenary assembly.
  * Attend the Consortium annual meeting during which the ns-3 most recent release shall be presented and during which a sample of suggestions made by Consortium Members shall be discussed.
  * Designate attendees (1 for small companies and universities and 2 for large companies) to attend a yearly, one day ns-3 training course.
  * Receive yearly Consortium financial and technical summary report.
  * Have their name, including logo, placed on the ns-3 website www.nsnam.org.

To inquire about participating, please email <consortium@nsnam.org>.
