---
title: Steering Committee Meetings
layout: page
permalink: /consortium/activities/steering-committee-meetings/
---
This page lists summary meeting minutes of the Steering Committee meetings:

  * <a target="_blank" href="http://www.nsnam.org/docs/consortium/meetings/annual-meeting-minutes-2016.pdf">June 16, 2016 (annual meeting)</a>
  * <a target="_blank" href="http://www.nsnam.org/docs/consortium/meetings/minutes-051216-posted.txt">May 12, 2016</a>
  * <a target="_blank" href="http://www.nsnam.org/docs/consortium/meetings/minutes-102115-posted.txt">October 21, 2015</a>
  * <a target="_blank" href="http://www.nsnam.org/docs/consortium/meetings/annual-meeting-notes-2015.pdf">May 14, 2015 (annual meeting)</a>
  * <a target="_blank" href="http://www.nsnam.org/docs/consortium/meetings/minutes-012315-posted.txt">January 23, 2015</a>
  * <a target="_blank" href="http://www.nsnam.org/docs/consortium/meetings/minutes-101014-posted.txt">October 10, 2014</a>
  * <a target="_blank" href="http://www.nsnam.org/docs/consortium/meetings/minutes-091614-posted.txt">September 16, 2014</a>
  * <a target="_blank" href="http://www.nsnam.org/docs/consortium/meetings/annual-meeting-notes-2014.pdf">May 8, 2014 (annual meeting)</a>
  * <a target="_blank" href="http://www.nsnam.org/docs/consortium/meetings/minutes-021414-posted.txt">Feburary 13, 2014</a>
  * <a target="_blank" href="http://www.nsnam.org/docs/consortium/meetings/minutes-112013-posted.pdf">November 20, 2013</a> 
  * <a target="_blank" href="http://www.nsnam.org/docs/consortium/meetings/minutes-092613-posted.txt">September 26, 2013</a> 
  * <a target="_blank" href="http://www.nsnam.org/docs/consortium/meetings/minutes-030513-posted.txt">March 5, 2013</a>
