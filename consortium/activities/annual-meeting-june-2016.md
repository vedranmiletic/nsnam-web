---
title: Annual Meeting June 2016
layout: page
permalink: /consortium/activities/annual-meeting-june-2016/
---
The ns-3 Consortium held its fourth annual meeting during the week of June 13-17, 2016, in Seattle, Washington, hosted by the University of Washington. Sixty-five attendees participated over the week. Meeting summaries and outputs are provided below.

  * A team of ns-3 maintainers presented two days of [training](https://www.nsnam.org/consortium/activities/training/) on June 13-14, covering an overview of the simulator, and specific focus on WiFi and LTE models, distributed simulations, emulation, and Direct Code Execution. 
      * The 8th annual [Workshop on ns-3](http://www.nsnam.org/overview/wns3/wns3-2016/) was held on Wednesday June 15 and Thursday June 16, featuring eighteen paper presentations, and thirteen posters and demos. 
          * The annual plenary meeting of the consortium was held on Thursday June 16, 2016 (minutes posted [here](http://www.nsnam.org/docs/consortium/meetings/annual-meeting-notes-2016.pdf)). 
              * A special Workshop on Wireless Network Performance Evaluation (WNPE) was held on Friday June 17, 2016 ([workshop website](https://www.ee.washington.edu/events/wns3_2016/wnpe_program.html)). </ul>
