---
title: Annual Meeting March 2013
layout: page
permalink: /consortium/activities/annual-meeting-march-2013/
---
The launch day of the NS-3 Consortium was held on Monday, March 4, 2013 at [INRIA in Sophia Antipolis](http://www.inria.fr/centre/sophia/).

Plenary lectures and tutorials, open to all users and developers interested in network simulation, were presented to allow attendees to discover or deepen their knowledge of ns-3, as well as better understand the objectives of the consortium.

## Agenda

The agenda was organized as follows. The main focus of the morning talks was to provide a high-level description of the goals of the consortium, the goals of the ns-3 open source project, and a summary of some current activities of possible interest. The afternoon sessions were more technical in nature, and ran in two parallel tracks. One track offered a tutorial for ns-3 beginners, and the second track offered more in-depth tutorials to those already familiar with ns-3.

  * Plenary talks (09h00-12h00) 
      * **Introduction to the NS-3 Consortium (Tom Henderson and Walid Dabbous)  [(slides)](http://www.nsnam.org/tutorials/consortium13/ns-3-consortium-introduction.pdf)** This talk introduces both the ns-3 project and consortium, with a focus on the long-term goals of both organizations.
      * **The LENA Project (Nicola Baldo)  [(slides)](http://www.nsnam.org/tutorials/consortium13/baldo-consortium-slides.pdf)** Ubiquisys, a developer of intelligent cells, and the Centre Tecnològic de Telecomunicacions de Catalunya (CTTC), a leading research organization in communication technologies, are working together to develop the world’s first open source product-oriented LTE network simulator, based on ns-3.
      * **Direct Code Execution with ns-3 (Mathieu Lacage and Hajime Tazaki)  [(slides)](http://www.nsnam.org/tutorials/consortium13/lacage-tazaki-consortium-slides.pdf)** Direct Code Execution (DCE) is a module for NS-3 that provides facilities to execute, within ns-3, existing implementations of userspace and kernelspace network protocols or applications without source code changes. For example instead of using the pseudo application provided by NS-3 V4PingHelper you can use the true ping. This talk provides an overview of the DCE framework and its applications.
      * **Using Network Simulation in Classroom Education (George Riley)  [(slides)](http://www.nsnam.org/tutorials/consortium13/riley-consortium-slides.pdf)** This talk focuses on the application of ns-3 in a graduate-level course on computer networking.
      * **The Evolution of a Computer Aided Simulation System (Felipe Perrone)  [(slides)](http://www.nsnam.org/tutorials/consortium13/perrone-consortium-slides.pdf)** This talk highlights ongoing work on developing scripts and infrastructure for automating ns-3 experiments.
  * Lunch (courtesy of INRIA) (12h00-13h00)
  * Tutorial Session 1 (13h00-15h00) 
      * 13h00-17h00: Track 1:  **ns-3 introductory tutorial  [(slides)](http://www.nsnam.org/tutorials/consortium13/ns-3-tutorial-consortium.pdf)**
      * 13h00-14h00: Track 2: **A technical overview of the ns-3 LTE
  
        module by the LENA project  [(slides)](http://www.nsnam.org/tutorials/consortium13/lte-tutorial.pdf)** This advanced tutorial goes into technical detail regarding the LTE simulation module within ns-3.
      * 14h00-15h00: Track 2: **Direct Code Execution  [(slides)](http://www.nsnam.org/tutorials/consortium13/dce-tutorial.pdf)** This tutorial walks through the usage of the DCE framework.
  * Tutorial Session 2 (15h30-17h00) 
      * 15h30-17h00: Track 1: ns-3 introductory tutorial (see above for slides)
      * 15h30-16h15: Track 2: **NEPI: Network Experiment Programming Interface  [(slides)](http://www.nsnam.org/tutorials/consortium13/nepi-tutorial.pdf)** ns-3 was designed to be integrated with testbed and virtual machine-based experimental frameworks. This talk provides an overview of a framework for network experiment management, including hybrid testbed and simulation configurations.
      * 16h15-17h00: Track 2: **Data collection and visualization  [(slides)](http://www.nsnam.org/tutorials/consortium13/visualization-tutorial.pdf)**. This tutorial introduces the audience to several tools used to extract and visualize data from ns-3 simulations, including the flow monitor, network animator, Python-based visualizer, and the ns-3 tracing system.
  * Cocktail (17h00-18h00)

Coffee breaks will additionally be provided.

## Other events this week

  * Information about Tuesday&#8217;s  [Workshop on ns-3](/research/wns3/wns3-2013/)
  * Information about  [the developers meeting](/wiki/DevelMeetingMar2013) (Wednesday-Friday)
