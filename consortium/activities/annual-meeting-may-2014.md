---
title: Annual Meeting May 2014
layout: page
permalink: /consortium/activities/annual-meeting-may-2014/
---
The ns-3 Consortium held its second annual meeting during the week of May 5-9, 2014 in Atlanta GA, hosted by [Georgia Institute of Technology](http://www.ece.gatech.edu/). Roughly thirty attendees participated over the week. Meeting summaries and outputs are provided below.

  * A team of ns-3 maintainers presented two days of training on May 5-6, covering an overview of the simulator, and specific focus on ns-3 tracing, WiFi and LTE models, distributed simulations, emulation, and Direct Code Execution. 
  * The 6th annual [Workshop on ns-3](/research/wns3/wns3-2014/) was held on Wednesday May 7, featuring eight paper presentations, five posters, and a keynote talk. 
  * The annual plenary meeting of the consortium was held on Thursday May 8, 2014 (minutes posted [here](/docs/consortium/meetings/annual-meeting-notes-2014.pdf)). 
  * ns-3 developer meetings were held following the consortium discussions (minutes posted [here](/docs/meetings/ns-3-developer-meeting-notes-May14.pdf)). 

An overview of the meeting can be found in [this flyer](http://www.nsnam.org/wp-content/uploads/2014/02/ns-3-annual-2014.pdf).
