---
title: Annual Meeting May 2015
layout: page
permalink: /consortium/activies/annual-meeting-may-2015/
---
The ns-3 Consortium held its third annual meeting during the week of May 11-15, 2015 in Castelldefels (Barcelona), Spain, hosted by [CTTC](http://cttc.es/). Roughly sixty attendees participated over the week. Meeting summaries and outputs are provided below.

  * A team of ns-3 maintainers presented two days of [training](/consortium/activities/training/) on May 11-12, covering an overview of the simulator, and specific focus on vehicular simulations, WiFi and LTE models, distributed simulations, emulation, and Direct Code Execution. 
  * The 7th annual [Workshop on ns-3](/overview/wns3/wns3-2015/) was held on Wednesday May 13 and Thursday May 14, featuring seventeen paper presentations, and thirteen posters and demos. 
  * The annual plenary meeting of the consortium was held on Thursday May 14, 2015 (minutes posted [here](/docs/consortium/meetings/annual-meeting-notes-2015.pdf)). 
  * ns-3 developer meetings were held following the consortium discussions (minutes posted [here](/docs/meetings/ns-3-developer-meeting-notes-May15.pdf)). 

An overview of the meeting can be found in [this flyer](/wp-content/uploads/2015/02/ns-3-annual-2015.pdf).
