---
title: Software
layout: page
permalink: /consortium/activities/software/
---
The Consortium can sponsor software development activities that benefit the open source project.

  * The port of [ns-3 for Visual Studio 2012](http://www.nsnam.org/wiki/Ns-3_on_Visual_Studio_2012) was organized by the Consortium in 2013.
