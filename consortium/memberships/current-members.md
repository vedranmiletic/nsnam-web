---
title: Current members
layout: page
permalink: /consortium/memberships/current-members/
---
**The Founding Executive Members are:**

<a href="http://www.inria.fr/centre/sophia/" target="_blank"><img src="/wp-content/uploads/2013/01/inr_logo_cherch_UK_coul.png" alt="INRIA" /></a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://www.ee.washington.edu" target="_blank"><img src="/wp-content/uploads/2013/01/University_of_Washington_Seal-100pixels.png" alt="University of Washington" /></a> 

**Additional Executive Members are:**

<a href="http://www.cttc.es" target="_blank"><img src="/wp-content/uploads/2013/02/CTTC_logo_square.png" alt="CTTC" /></a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://www.inescporto.pt/" target="_blank"><img src="/wp-content/uploads/2013/02/INESC-TEC-logo_png.png" alt="INESC Porto" /></a> 

<a href="http://www.bucknell.edu/Engineering.xml" target="_blank"><img src="/wp-content/uploads/2013/09/BUWordmark.png" width="150" alt="Bucknell University" /></a>&nbsp;&nbsp;&nbsp;<font size="4"><a href="http://www.ece.gatech.edu/" target="_blank">Georgia Institute of Technology</a></font> 

**Consortium Members:**

<a href="http://www.llnl.gov/" target="_blank"><img src="/wp-content/uploads/2013/10/llnl-logo.gif" width="250" alt="Lawrence Livermore National Laboratory"></a> 

<a href="http://www.nitk.ac.in/" target="_blank"><img src="/wp-content/uploads/2016/04/NITK_100_by_100.png" alt="NITK Surathkal" /></a>&nbsp;&nbsp;<a href="http://www.nitk.ac.in/" target="_blank"><b><font size="3">NITK Surathkal</font></b></a> 

<a href="http://www.nitk.ac.in/" target="_blank"><img src="/wp-content/uploads/2018/03/Hustseals_100x100.png" alt="HUST" /></a>&nbsp;&nbsp;<a href="http://english.hust.edu.cn/" target="_blank"><b><font size="3">Huazhong University of Science and Technology</font></b></a> 

[**<font size="3">CMMB Vision</font>**](http://www.cmmbvision.com/) 

<a href="https://www.cablelabs.com" target="_blank"><img src="/wp-content/uploads/2018/09/CableLabs-logo-scaled-225.png" alt="CableLabs" /></a>&nbsp;&nbsp;<a href="https://www.cablelabs.com" target="_blank"><b><font size="3">CableLabs</font></b></a>
