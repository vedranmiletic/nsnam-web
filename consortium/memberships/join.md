---
title: Join
layout: page
permalink: /consortium/memberships/join/
---
Organizations may become Consortium Members, upon approval by the Steering Committe, by submitting an application agreement ([docx](/wp-content/uploads/2017/08/ns-3-Consortium-member-application-form.docx)&nbsp;or&nbsp;[pdf](/wp-content/uploads/2017/08/ns-3-Consortium-member-application-form.pdf)&nbsp;or &nbsp;[odt](/wp-content/uploads/2017/08/ns-3-Consortium-member-application-form.odt)) to the mailing address at <consortium@nsnam.org> and agreeing to pay the annual fees associated with membership.&nbsp; The fee structure is as follows:

University of Washington-based Consortium Members

  * Universities, non-profits, FFRDCs: $1,500
  * Very small companies (fewer than 20 employees): $1,500
  * Small companies (20-500 employees): $7,500
  * Large companies (greater than 500 employees): $15,000
		  
    &nbsp;

INRIA-based Consortium Members:

  * Universities and non-profit: &euro;1,176 (plus any applicable VAT)
  * Very small companies (fewer than 20 employees): &euro;1,176 (plus any applicable VAT)
  * Small companies (20-500 employees): &euro;5,882 (plus any applicable VAT)
  * Large companies (greater than 500 employees): &euro;11,765 (plus any applicable VAT)
