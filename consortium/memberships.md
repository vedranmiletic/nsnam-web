---
title: Memberships
layout: page
permalink: /consortium/memberships/
---
Memberships are of two types:&nbsp; **_Executive Members_** and **_Consortium Members_**.

INRIA and the University of Washington are Founding Executive Members of the Consortium. INRIA and University of Washington may mutually decide to invite further Executive Members to join the NS3 Consortium.&nbsp; Executive Members have the authority to appoint the Steering Committee that governs the consortium.

Consortium Members are of four kinds:

  * Universities, non-profits, FFRDCs
  * Very small companies (fewer than 20 employees)
  * Small companies (20-500 employees)
  * Large companies (greater than 500 employees)

Prospective Consortium Members may initiate a request to join the Consortium by signing and submitting to the relevant Executive Member an Application Form. In signing, the joining Consortium Member makes a formal statement of support for the mission and vision of the Consortium and makes a statement of commitment to active participation in relevant activities. Membership is subject to approval by the Steering Committee.
	  
