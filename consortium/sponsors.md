---
title: Sponsors
layout: page
permalink: /consortium/sponsors/
---
This page recognizes those individuals and organizations who have donated funding to the Consortium as an unrestricted gift.

* [CTTC](http://www.cttc.es) mentoring team (Nicola Baldo, Marco Miozzo) for [GSoC 2012](http://www.google-melange.com/gsoc/homepage/google/gsoc2012)
  
* [CTTC](http://www.cttc.es) mentoring team (Nicola Baldo, Marco Miozzo, Manuel
  
Requena, and Jaime Ferragut) for [GSoC 2013](http://www.google-melange.com/gsoc/homepage/google/gsoc2013)
  
* Daniel Camara
  
* Joe Kopena
  
* Tommaso Pecorella
  
* Josh Pelkey
  
* Guillaume Rémy
