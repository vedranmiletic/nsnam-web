---
title: Activities
layout: page
permalink: /consortium/activities/
---
The main activities are focused on promoting the development and maintenance of ns-3 software.  However, the ns-3 project operates separately as an open source project.

The activities that the Consortium oversees are as follows:

 * prepare a yearly scientific, technical, and financial report. 
 * organize an annual meeting for Consortium members; this meeting may coincide with a technical conference or may be an independent event.  This event is typically the annual Workshop on ns-3. 
 * organize and provide for an annual training course for Consortium members, typically held with the annual Workshop on ns-3.
 * administrative items dealing with Consortium finances and project maintenance activities.
 * recommend or fund software development, tests, or documentation based on member contributions.
