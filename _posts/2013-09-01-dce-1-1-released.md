---
id: 2487
title: DCE 1.1 released
date: 2013-09-01T03:55:24+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=2487
permalink: /news/dce-1-1-released/
categories:
  - Events
  - News
---
Version 1.1 of the [Direct Code Execution (DCE) environment](http://www.nsnam.org/overview/projects/direct-code-execution/dce-1-1/) was released on September 1. Compatible with the [ns-3.18](http://www.nsnam.org/ns-3-18/) release, DCE 1.1 adds a few new features:
  
* [Aspect-based tracing](http://www.nsnam.org/docs/dce/release/1.1/manual/html/dce-user-aspect-trace.html)
  
* [LTE (LteUeNetDevice) Support with multipath TCP](https://plus.google.com/106093986108036190339/posts/aYuboVFcQeF)
  
* numerous bug fixes