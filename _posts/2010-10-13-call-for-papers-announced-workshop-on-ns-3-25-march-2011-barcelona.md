---
id: 203
title: 'Call for Papers: Workshop on ns-3, 25 March 2011, Barcelona'
date: 2010-10-13T16:06:05+00:00
author: tomh
layout: post
guid: http://www2.nsnam.org/?p=203
permalink: /events/call-for-papers-announced-workshop-on-ns-3-25-march-2011-barcelona/
categories:
  - Events
---
The [WNS3&#8217;11](http://wns3.org/2011/) workshop will be collocated with SIMUTOOLS. Authors interested in submitting a paper should consult the [Call for Papers](http://www.wns3.org/2011/Publicity/CallForPapers)