---
id: 2739
title: WNS3 Call for Posters and Demos
date: 2014-02-19T15:00:53+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=2739
permalink: /events/wns3-call-for-posters-and-demos/
categories:
  - Events
---
The [Workshop on ns-3 (WNS3)](http://www.nsnam.org/wns3/wns3-2014/) invites your participation in the workshop with a short presentation about work-in-progress, a poster presentation, and/or a demo presentation. We are planning to organize events in addition to the regular paper track, to create more opportunities for discussion in the workshop. The call for posters and demonstrations can be found [here](http://www.nsnam.org/wns3/wns3-2014/call-for-postersdemos/).

WNS3 will be part of a [week-long series of events](http://www.nsnam.org/consortium/activities/annual-meeting-may-2014/) in Atlanta GA including ns-3 training, the consortium annual meeting, and developer meetings.