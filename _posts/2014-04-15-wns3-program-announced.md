---
id: 2801
title: WNS3 program announced
date: 2014-04-15T05:09:23+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=2801
permalink: /events/wns3-program-announced/
categories:
  - Events
---
The program for the upcoming annual Workshop on ns-3 has been finalized and posted on the [workshop web page](http://www.nsnam.org/overview/wns3/wns3-2014/). The Workshop, held on May 7 2014 in Atlanta GA, features a [keynote address](http://www.nsnam.org/overview/wns3/wns3-2014/keynote/) by Prof. Ellen W. Zegura, eight [regular paper presentations](http://www.nsnam.org/overview/wns3/wns3-2014/accepted-papers/), and five [poster presentations](http://www.nsnam.org/overview/wns3/wns3-2014/accepted-postersdemos/). Registration details can be found [here](http://www.nsnam.org/overview/wns3/wns3-2014/registration/).