---
id: 2451
title: ns-3.18 release
date: 2013-08-29T23:08:02+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=2451
permalink: /news/ns-3-18-release/
categories:
  - Events
  - News
  - ns-3 Releases
---
The ns-3.18 release has now been posted at <http://www.nsnam.org/release/ns-allinone-3.18.tar.bz2>.

This release features the latest LTE module code from the LENA project, including improved support for UE measurements, and enhancements to the buildings module to enable the use of any mobility model. IEEE 802.11n features have been added to the WiFi module; it is possible to create a high throughput (HT) node that uses the 11n data rates and preambles. This release also adds new data collection components enabling the plotting and file outputs from ns-3 trace sources, and a new generic hash function interface in the simulation core. In addition, work on several aspects to improve IPv6 support has been added, including Path-MTU detection and a new helper for the radvd daemon.