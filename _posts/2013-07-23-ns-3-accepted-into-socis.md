---
id: 2429
title: ns-3 accepted into SOCIS
date: 2013-07-23T13:26:43+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=2429
permalink: /uncategorized/ns-3-accepted-into-socis/
categories:
  - Uncategorized
---
ns-3 has been accepted to the 2013 edition of the [European Space Agency Summer of Code in Space (SOCIS) program](http://sophia.estec.esa.int/socis2013/). The program is inspired by the Google Summer of Code program, but with a space-based theme. Student [applications](http://sophia.estec.esa.int/socis2013/?q=node/21) are due on August 4, 2013. Please visit the ns-3 [project ideas page](http://www.nsnam.org/wiki/SOCIS2013Projects).
