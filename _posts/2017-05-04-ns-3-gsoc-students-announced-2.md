---
id: 3726
title: ns-3 GSoC students announced
date: 2017-05-04T21:03:19+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=3726
permalink: /news/ns-3-gsoc-students-announced-2/
categories:
  - News
---
We're pleased to announce that we have been awarded and have selected five student projects for the 2017 edition of Google Summer of Code.&nbsp; The students and projects selected are:

  * Abhijith Anilkumar, "ns-3 App Store," mentored by Tom Henderson
  * Shravya K.S., "Framework for TCP Prague simulations in ns-3," mentored by Mohit Tahiliani
  * Alexander Krotov, "Enabling LTE CA handover to secondary cell," mentored by Biljana Bojovic
  * Manoj Kumar Rana, "Mobile IPv6 Implementation with LTE support," mentored by Tommaso Pecorella
  * Surya Seetharaman, "Implementation of Token Bucket Filter and Heavy-Hitter Filter in ns-3," mentored by Stefano Avallone

This year's application process was especially competitive, as 17 applicants successfully completed our application, and we received more worthy proposals than we could accommodate.&nbsp; The projects selected were those that scored the best in overall application quality, alignment with available mentors, and open source track record including ns-3 experience.