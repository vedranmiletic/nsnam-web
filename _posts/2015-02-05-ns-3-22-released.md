---
id: 3076
title: ns-3.22 released
date: 2015-02-05T21:46:30+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=3076
permalink: /news/ns-3-22-released/
categories:
  - News
  - ns-3 Releases
---
[ns-3.22](http://www.nsnam.org/ns-3.22) provides a number of updates related to WiFi support, including MPDU aggregation for WiFi, and new WiFi rate controls. Related to WiFi, the WAVE module (for vehicular networks) adds the channel-access coordination features of IEEE 1609.4, and a comprehensive VANET routing example that includes a Basic Safety Message (BSM) packet generator. Regarding LTE models, in previous releases, the bearer release functionality was only partially supported; a complete release bearer procedure is now implemented. Other details, including a list of all bugs fixed since the last release, can be found in the notes linked from the above page.