---
layout: page
title: Documentation
permalink: /documentation/
---

This page is an entry point to the ns-3 documentation maintained by the project (other tutorials exist on external blogs and on YouTube but are not typically listed here).

Documentation is available for the current release ([ns-3.29](/releases/ns-3-29/)), [older releases](/documentation/older/), and also our [development tree](/documentation/development-tree/).

## Getting started

*   **Tutorial:** The tutorial for our latest release is available in [HTML](/docs/release/3.29/tutorial/html/index.html) and [PDF](/docs/release/3.29/tutorial/ns-3-tutorial.pdf) versions.
*   **Installation instructions:** We typically maintain this information on our [wiki](/wiki/Installation).
*   **Videos:** The ns-3 Consortium has offered training sessions at its annual meeting ([next scheduled for June 2019](/overview/wns3/)), and recorded videos from prior tutorials can be found from [this page](/consortium/activities/training/). The first such sessions are introductory surveys for new users.
*   **Mailing lists:** We have several [mailing lists](/support/mailing-list/), but in particular, the [ns-3-users Google Group forum](https://groups.google.com/forum/#!forum/ns-3-users), answers many questions from people trying to get started.

## Development

Most users will need to write new simulation scripts and possibly modify or extend the ns-3 libraries to conduct their work. The three main resources for this are our reference manual, model library documentation, and our Doxygen.

*   We maintain a **reference manual** on the ns-3 core, and a separate **model library** documentation set, also in [several formats](/ns-3-29/documentation/) for our latest release.
*   All of our APIs are documented using [**Doxygen**](/docs/release/3.29/doxygen/index.html)
*   The ns-3 [coding style](/developers/contributing-code/coding-style/) documentation is maintained on this site.

## Related projects

*   **Direct Code Execution**: Documentation on the ns-3 [Direct Code Execution](/overview/projects/direct-code-execution) environment is also linked from this site.
*   **[Netanim](/wiki/NetAnim)**: A network animator for ns-3.
*   **[Bake](/docs/bake/tutorial/html/)**: The package management tool for advanced ns-3 builds.
*   **[ns-2](/support/faq/ns2-ns3/)**

## Miscellaneous

Not finding what you are looking for? Have a look at the [ns-3 wiki](/wiki), where such topics as current development, release planning, summer projects, contributed code, FAQs and HOWTOs. Not everything is linked from the masthead so try entering keywords in the search box.

We have a number of [**other archived documents**](/documentation/presentations/) such as older tutorials or talks presented about ns-3.
