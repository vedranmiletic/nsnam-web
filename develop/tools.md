---
title: Tools
layout: page
permalink: /develop/tools/
---
To download, manage the source code history, track the bugs, build, and review patches, we use many tools that were not developed for ns-3. If you are already familiar with them, the following list should give you a good idea of what can be found where.

  * [Git](/develop/tools/git)
  * [Waf build system](/develop/tools/waf)
  * [Bugzilla](/develop/tools/bugzilla)
  * [code reviews via Rietveld](/develop/tools/rietveld)
  * [ns-3 mailing-lists](/develop/tools/mailing-lists)
  * [continuous integration](/develop/tools/buildbot)
  * [chat](/develop/tools/chat)
  * [Wiki](https://www.nsnam.org/wiki)
