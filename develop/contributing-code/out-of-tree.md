---
title: Out of tree
layout: page
permalink: /develop/contributing-code/out-of-tree/
---
# Submitting out-of-tree code

Anyone who wants to provide access to code that has been developed to extend ns-3, but who chooses not to go through the above review process, may list its availability on our website. Furthermore, we will provide storage on a web server if needed. This type of code contribution will not be formally maintained by the project, although popular extensions might be adopted downstream.

We ask anyone who wishes to do this to provide at least this information on our wiki:

  * Authors,
  * Name and description of the extension,
  * How it is distributed (as a patch or full tarball),
  * Location,
  * Status (how it is maintained)

The contribution will be stored on our wiki <a href="http://www.nsnam.org/wiki/Contributed_Code" target="_blank">here</a>. If you need web server space for your extension, please contact one of the project maintainers.
