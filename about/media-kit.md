---
title: Media kit
layout: page
permalink: /about/media-kit/
---
If you need to reference the ns-3 project, the following images are available for use under CC-BY-SA-4.0 license:

  * Color logo with text _Network Simulator_: <a href="/wp-content/uploads/2011/05/ns-3.png" target="_blank">png</a>, <a href="/wp-content/uploads/2011/05/ns-3.pdf" target="_blank">pdf</a>.
  * Color inverted logo with text _Network Simulator_: <a href="/wp-content/uploads/2011/05/ns-3-inverted.png" target="_blank">png</a>, <a href="/wp-content/uploads/2011/05/ns-3-notext.pdf" target="_blank">pdf</a>.
  * Color logo without text: <a href="/wp-content/uploads/2011/05/ns-3-notext.png" target="_blank">png</a>, <a href="/wp-content/uploads/2011/05/ns-3-notext.pdf" target="_blank">pdf</a>.
  * Color inverted logo without text: <a href="/wp-content/uploads/2011/05/ns-3-inverted-notext.png" target="_blank">png</a>, <a href="/wp-content/uploads/2011/05/ns-3-inverted-notext.pdf" target="_blank">pdf</a>.

The font used for the text _ns-3_ is _Aldo_.

The green color used for the radio bars can be obtained with the rgb tripplet _99d420_ (_153,212,32_ in decimal) or CMYK tripplet 28,0,85,17
