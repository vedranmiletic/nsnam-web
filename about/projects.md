---
title: Projects
layout: page
permalink: /about/projects/
---
Aside from the core ns-3 simulator, several projects are hosted at nsnam.org or are closely associated with ns-3, and are listed below.

  * [Direct Code Execution (DCE)](/about/projects/direct-code-execution/): a framework to execute real kernel and application code from within ns-3
  * [netanim](/wiki/NetAnim): an offline animator based on the Qt 4 toolkit
  * [pybindgen](https://github.com/gjcarneiro/pybindgen): a tool to generate Python bindings for C/C++ code
  * [bake](/docs/bake/tutorial/html/index.html): a build tool for ns-3 </ul> 
    Additional related projects can be found on our [wiki](/wiki//Related_Projects).
